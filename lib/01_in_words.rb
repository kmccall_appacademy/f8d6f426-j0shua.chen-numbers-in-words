


  ONES ={
    0=> "zero",
    1=> "one",
    2=> "two",
    3=>"three",
    4=>"four",
    5=>"five",
    6=>"six",
    7=>"seven",
    8=>"eight",
    9=> "nine",

  }

  TENS ={
    20=>"twenty",
    30=>"thirty",
    40=>"forty",
    50=>"fifty",
    60=>"sixty",
    70=>"seventy",
    80=>"eighty",
    90=>"ninety"
  }

  TEENS ={
    10=> "ten",
    11=> "eleven",
    12=> "twelve",
    13=> "thirteen",
    14=> "fourteen",
    15=> "fifteen",
    16=> "sixteen",
    17=> "seventeen",
    18=> "eighteen",
    19=> "nineteen"
  }

  SCALE ={
    100=> "hundred",
    1000 => "thousand",
    1000000=> "million",
    1000000000 =>"billion",
    1000000000000 =>"trillion",

  }

class Fixnum
  def in_words
    if self < 10
      ONES[self]
    elsif self <20
      TEENS[self]
    elsif self <100
      tenz = TENS[self/10 *10]
      if self %10 ==0
        tenz
      else
        tenz + " " + (self%10).in_words
      end
    else
      num = self.number
      num_w = (self/num).in_words + " " + SCALE[num]
      if self % num == 0
        num_w
      else
        num_w +" " +(self%num).in_words
      end
    end
  end

  def number
    SCALE.keys.select {|n| n <=self}.last
  end

end
